<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
class PostsController extends Controller
{
    public function createpost()

    {

        return view('posts.create-post');

    }
        
        
    public function store(Request $request)
    {
        
        $post = new Post;
        $post->title=$request->title;
        $post->body=$request->body;
        $post->save();
        return route('post.info', $post->id);
        
    }
    public function updatepost(Request $request)
    {
        
        $posts = Post::all();
        return view('users.successlogin',['posts'=>$posts] );
        
    }
    public function displaypost(Request $request)
    {
        $posts = Post::all();
        return view('pages.home',['posts' => $posts]);
    }

    public function postinfo(Post $id)
    {
        $posts = Post::find($id);
        return view('posts.post')->with('posts', $posts);
    }

   
}
