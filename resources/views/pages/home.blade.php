<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
   .nav-link{
     text-align:center;
   }
   .card-body{
    text-align:center;
   }
   .b{
    text-align:center;
   }
  
  </style>

  <title>صفحه اصلی</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/blog-home.css" rel="stylesheet">
  <strong class="b">
    <a href="/create-user">ثبت نام</a><br>
    <a href="/main">ورود</a><br>

    <a href="/">خانه</a><br><br><br>
   </strong>
</head>

<body>@foreach($posts as $post)
<div class="card mb-4">
          
          <div class="card-body">
          
            <h2 class="card-title">{{$post->title}}</h2>
            
            <a href="{{ route('post.info', $post->id) }}" class="btn btn-primary">Read More &rarr;</a>
          </div>
@endforeach 
</html>
