<!DOCTYPE html>
<html>
 <head>
  <title>ساخت حساب</title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container box">
   <h3 align="center">ثبت نام</h3><br/>

   

   @if (count($errors) > 0)
    <div class="alert alert-danger">
     <ul>
     @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
     @endforeach
     </ul>
    </div>
   @endif

   <form method="post" action="">
    {{ csrf_field() }}
    <div class="form-group">
     <label>name</label>
     <input type="name" name="name" class="form-control" />
    </div>
    <div class="form-group">
     <label>email</label>
     <input type="email" name="email" class="form-control" />
    </div>
    <div class="form-group">
     <label>password</label>
     <input type="password" name="password" class="form-control" />
    </div>
    <div class="form-group">
     <input type="submit" name="register" class="btn btn-primary" value="ساخت حساب" />
    </div>
   </form>
  </div>
 </body>
</html>
