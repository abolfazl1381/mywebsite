@section('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
  </style>

<body>
  <br />
  <div class="container box">
   <h3 align="center">ارسال پست</h3><br/>

   

   @if (count($errors) > 0)
    <div class="alert alert-danger">
     <ul>
     @foreach($errors->all() as $error)
      <li>{{ $error }}</li>
     @endforeach
     </ul>
    </div>
   @endif

   <form method="post" action="/post-save">
    {{ csrf_field() }}
    <div class="form-group">
     <label>title</label>
     <input type="title" name="title" class="form-control" />
    </div>
    <div class="form-group">
     <label>body</label>
     <input type="body" name="body" class="form-control" />
    </div>
    <div class="form-group">
     <input type="submit" name="post" class="btn btn-primary" value="submit" />
    </div>
   </form>
  </div>
 </body>