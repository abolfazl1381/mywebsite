<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
 
  

  <title>Blog Post - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/blog-post.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      
     
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="{{url ('/')}}">خانه
              
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url ('/create-user')}}">ثبت نام</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url ('/main')}}">ورود</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <style type="text/css">
  
   .body{
    text-align:center;
   }
  
  </style>

<div class="body">
@foreach($posts->all() as $p)
<h1>{{$p->body}}</h1>

</div>
@endforeach

<div class="card-body">
                <h5>Display Comments</h5>
                <hr />
                @foreach($posts as $post)
                @include('posts.replay', ['comments' => $post->comments, 'post_id' => $post->id])
                @endforeach
                
                <hr />
               </div>

               <div class="card-body">
                <h5>Leave a comment</h5>
                <form method="post" action="{{ route('comment.add') }}">
                    @csrf
                    <div class="form-group">
                        <input type="text" name="author" class="form-control" placeholder="name"/>
                        <input type="text" name="comment" class="form-control" placeholder="comment" />
                        @foreach($posts as $post)
                        <input type="hidden" name="post_id" value="{{ $post->id }}" />
                        @endforeach
                    </div>
                    <div class="form-group">
                        <input type="submit" class="btn btn-sm btn-outline-danger py-0" style="font-size: 0.8em;" value="Add Comment" />
                    </div>
                </form>
               </div>

               

