<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/about',function(){

    return view('pages.about');
});

Route::get('/user/{id}',function($id){

    return $id;
});

Route::get('/', 'App\Http\Controllers\PostsController@displaypost');

Route::post('/post-save',[App\Http\Controllers\PostsController::class, 'store']);
Route::view('/post-create','posts.create-post');
Route::view('/create-user','users.usercreate');

Route::post('/create-user','App\Http\Controllers\UsersController@adduser');

Route::get('main/successlogin', 'App\Http\Controllers\UsersController@successlogin');
Route::get('main/update/{id}', 'App\Http\Controllers\PostsController@updatepost');
Route::get('main','App\Http\Controllers\UsersController@index');
Route::post('main/checklogin', 'App\Http\Controllers\UsersController@checklogin');
//Route::get('main/logout', 'App\Http\Controllers\UsersController@logout');
Route::get('/post/{id}','App\Http\Controllers\PostsController@postinfo')->name('post.info');

Route::post('/comment/store', 'App\Http\Controllers\CommentController@store')->name('comment.add');
Route::post('/reply/store', 'App\Http\Controllers\CommentController@replyStore')->name('reply.add');
    